import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

const SignUp = ({navigation}) => {

  return (
    <SafeAreaView style= {styles.safeAreaContent}>
      
      <ScrollView contentContainerStyle={styles.ScrollViewContent}>
      <Image source={require('./image.png')} style={styles.image} />
      <Text  style={styles.label}>Create an account</Text>

    <View style={styles.mainContent}>
      <TextInput style={styles.input} 
        placeholder='Name'
        keyboardType='words'
        placeholderTextColor={'#959595'}
      />
      <TextInput style={styles.input} 
        placeholder='Email'
        keyboardType='words'
        placeholderTextColor={'#959595'}
      />
      <TextInput style={styles.input} 
        placeholder='Phone'
        keyboardType='words'
        placeholderTextColor={'#959595'}
      />
      <TextInput style={styles.input}
         placeholder='Password'
         keyboardType='words'
         secureTextEntry={true}
         placeholderTextColor={'#959595'}
      />
      <TouchableOpacity style={styles.button}>
        <Text style={styles.ButtonName}>Sign up</Text>
           </TouchableOpacity>
         </View>
      </ScrollView>

      <View style={[styles.footerContent,]}>
        <Text>Already have account?{' '}
        <TouchableOpacity
        onPress={()=> navigation.navigate('SignIn') }>
        <Text style={[{fontWeight:'bold'}]}>Sign in</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('ForgotPassword')}></TouchableOpacity>
        </Text>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  image: {
    width: 250,
    height: 60,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginBottom: 30, 
  },

  safeAreaContent: {
    backgroundColor: 'red',
    flex: 1,
  },

  label: {
    fontSize: 20,
    paddingStart: 60,
    paddingBottom: 20,
  },

  input:{ 
    height: 48,
    margin: 9,
    borderWidth: 0.03,
    padding: 15,
    borderRadius: 0.19,
    paddingStart: 14,
    alignItems: 'center',
    marginHorizontal: 1,
    backgroundColor : 'white'
  },

  ButtonName: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 12,
  },

  button: {
    backgroundColor: '#2E3283',
    alignItems: 'center',
    borderRadius: 8,
    padding: 14,
  },

  ScrollViewContent: {
    backgroundColor: '#F4F4F4',
    paddingHorizontal: 44,
    paddingVertical: 48,
    flexGrow: 1,
    justifyContent: 'center',
  },

  footerContent: {
    backgroundColor: 'white',
    position: 'absolute',
    alignItems: 'center',
    bottom: 0,
    width: '100%',
    padding: 20,
    paddingStart: 20,
    paddingBottom: 20,
  },
  footerText: {
    fontSize: 12,
  },
}); 

export default SignUp;
