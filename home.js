

import React from 'react';
import {
  SafeAreaView,
  Image,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Button,
  NavigationContainer,
  createNativeStackNavigator,


} from 'react-native';

const home= () => {
  return (
    <SafeAreaView style= {styles.SafeAreaViewContent}>
    <Image source={require('./MarlinBanner.png')} style={styles.image} />
    <View style= {{ flexDirection:'row', marginTop: 15,justifyContent: 'center',}}>
        <Image source={require('./Ferry1.png')} style={{width: 72, height: 72, marginHorizontal: 10,}} />
        <Image source={require('./Ferry2.png')} style={{widht: 72, height: 72, marginHorizontal: 10,}} />
        <Image source={require('./Tenda.png')} style={{widht: 72, height: 72, marginHorizontal: 10,}} />
        <Image source={require('./BatasLaut.png')} style={{widht: 72, height: 72, marginHorizontal: 10,}} />
    </View>
    <View>
        <View style={styles.button}>
            <Text style={styles.ButtonName}>More ...</Text>
        </View>
    </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 155, 
      },
      ButtonName: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        fontSize: 16,
        marginTop: 15,
        fontFamily: 'nunito'
      },
      button: {
        backgroundColor: '#2E3283',
        alignItems: 'center',
        alignSelf: 'center',
        borderRadius: 8,
        paddingBottom: 20,
        position: 'absolute',
        bottom: 0,
        width: '100%',
        marginStart: 150,
        marginEnd: 150,
      },
   
});

export default home;