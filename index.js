/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import LoginUi from './SignIn';
import {name as appName} from './app.json';
import SignIn from './SignIn';
import SignUp from'./SignUp';
import ForgotPassword from'./ForgotPassword';
import home from'./home';
import NavigationContent from'./NavigationContent';

AppRegistry.registerComponent(appName, () => NavigationContent);
