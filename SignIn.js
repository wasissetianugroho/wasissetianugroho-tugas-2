import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} 
from 'react-native';

const SignIn = ({ navigation }) => {


  return (
    <SafeAreaView style= {styles.safeAreaContent}>
      
      <ScrollView contentContainerStyle={styles.ScrollViewContent}>
      <Image source={require('./image.png')} style={styles.image} />
      <Text style={styles.label}>Please sign in to continue</Text>

    <View style={styles.mainContent}>
      <TextInput style={styles.input} 
        placeholder='Username'
        keyboardType='words'
        placeholderTextColor={'#959595'}
      />
      <TextInput style={styles.input}
         placeholder='Password'
         keyboardType='words'
         secureTextEntry={true}
         placeholderTextColor={'#959595'}
      />
      <TouchableOpacity style={styles.button}>
        <Text style={styles.ButtonName}>Sign in</Text>
        </TouchableOpacity>
        <TouchableOpacity 
         onPress={()=> navigation.navigate('ForgotPassword') }>
       <Text style={styles.smallText}>Forgot password</Text>
        </TouchableOpacity>

        <View style={{flexDirection:'row',alignItems:'center'}}>
        <View style={styles.Line} />
        <Text style={{
          textAlign: 'center',
          paddingHorizontal: 16,
          fontSize: 11,
          color: '#2E3283',
        }}>
          Login with
        </Text>
         <View style={styles.Line} />
         </View></View>
         <View
         style={{
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          marginTop: 32,
        }}>
         <Image source={require('./Facebook.png')} />
         <Image source={require('./Google.png')} />
         </View>
         <Text style={{marginTop: 32, alignSelf:'center'}}>
          App Version {'             '}2.8.3
         </Text>
         <View></View>
      </ScrollView>

      <View style={ styles.footerContent}>
        <Text>Already have account?{' '}
        <TouchableOpacity
          onPress={()=> navigation.navigate('SignUp') }>
        <Text style={[{fontWeight:'bold'}]}>Sign up</Text>
        </TouchableOpacity>
        </Text>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  image: {
    width: 250,
    height: 60,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginBottom: 30, 
  },

  safeAreaContent: {
    backgroundColor: 'grey',
    flex: 1,
  },

  label: {
    fontSize: 20,
    paddingStart: 30,
    paddingBottom: 20,
  },

  input:{ 
    height: 48,
    margin: 9,
    borderWidth: 0.03,
    padding: 15,
    borderRadius: 0.19,
    paddingStart: 14,
    alignItems: 'center',
    marginHorizontal: 1,
    backgroundColor : 'white'
  },

  ButtonName: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 12,
  },

  button: {
    backgroundColor: '#2E3283',
    alignItems: 'center',
    borderRadius: 8,
    padding: 14,
    
  },
  ScrollViewContent: {
    paddingHorizontal: 44,
    paddingVertical: 48,
    flexGrow: 1,
    justifyContent: 'center',
    backgroundColor : '#F4F4F4'
  },

  footerContent: {
    backgroundColor: 'white',
    position: 'absolute',
    alignItems: 'center',
    bottom: 0,
    width: '100%',
    padding: 20,
    paddingStart: 20,
    paddingBottom: 20,
  },
smallText: {
  fontSize: 11,
    marginTop: 16,
    marginBottom: 32,
    alignSelf: 'center',
    color: '#2E3283',
},

Line: {
    flex: 1,
    height: 1,
    backgroundColor: '#959595',
  },

}); 

export default SignIn;
