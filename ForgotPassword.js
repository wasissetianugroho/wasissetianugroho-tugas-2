import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

const ForgotPassword = ({navigation}) => {

  return (
    <SafeAreaView style= {styles.safeAreaContent}>
      
      <ScrollView contentContainerStyle={styles.ScrollViewContent}>
      <Image source={require('./image.png')} style={styles.image} />
      <Text style={styles.label}>Reset your password </Text>

    <View style={styles.mainContent}>
      <TextInput style={styles.input} 
        placeholder='Email'
        keyboardType='words'
        placeholderTextColor={'#959595'}
      />
      <TouchableOpacity style={styles.button}>
        <Text style={styles.ButtonName}>Request Reset</Text>
           </TouchableOpacity>
         </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  image: {
    width: 250,
    height: 60,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginBottom: 30, 
  },

  safeAreaContent: {
    backgroundColor: 'grey',
    flex: 1,
  },

  label: {
    fontSize: 20,
    paddingStart: 60,
    paddingBottom: 20,
  },

  input:{ 
    height: 48,
    margin: 9,
    borderWidth: 0.03,
    padding: 15,
    borderRadius: 0.19,
    paddingStart: 14,
    alignItems: 'center',
    marginHorizontal: 1,
    backgroundColor : 'white',
  },

  ButtonName: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 17,
  },

  button: {
    backgroundColor: '#2E3283',
    alignItems: 'center',
    borderRadius: 8,
    padding: 14,
  },
  ScrollViewContent: {
    backgroundColor: '#F4F4F4',
    paddingHorizontal: 44,
    paddingVertical: 48,
    flexGrow: 1,
    justifyContent: 'center',
  },
}); 

export default ForgotPassword;
